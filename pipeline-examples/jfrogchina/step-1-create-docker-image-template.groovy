#!/usr/bin/env groovy
//参数化构建
//CREDENTIALS

import groovy.json.JsonSlurper

node {

// -------------------------------------------------------------------------------------------------------
// Parameters
    def REPO = "docker"                     //virtual docker repo
    def PROMOTE_REPO = "docker-prod-local"  //promotion target repo
    def SOURCE_REPO = "docker-stage-local"  //source target repo
    def NAMESPACEDOMAIN = "jfrogchina.local:8081"     //domain name to use: jfrog.local
    def SERVER_URL = "http://${NAMESPACEDOMAIN}/artifactory"

// Jenkins parameter 
//
// FAIL_BUILD: 'true' - fail build if violations are detected.  
// -------------------------------------------------------------------------------------------------------

    //Clone example project from GitHub repository
    git url: 'https://gitlab.com/zyun823/swampup2018.git'
    def rtServer = Artifactory.server "arti-demo"
    def buildInfo = Artifactory.newBuildInfo()
    def tagName
    def artdocker_registry = ${NAMESPACEDOMAIN}
    buildInfo.env.capture = true

    //Fetch all depensencies from Artifactory
    stage('Dependencies') {
        dir('project-examples/step2_dockertemplate') {
            try {
                println "Gather Java and Tomcat"
                def downloadSpec = """{
                         "files": [
                          {
                           "pattern": "tomcat-local-cache/java/jdk-8u91-linux-x64.tar.gz",
                           "target": "jdk/jdk-8-linux-x64.tar.gz",
                           "flat":"true"
                          },
                          {
                           "pattern": "tomcat-local-cache/org/apache/apache-tomcat/apache-tomcat-8.0.32.tar.gz",
                           "target": "tomcat/apache-tomcat-8.tar.gz",
                           "flat":"true"
                          }
                          ]
                        }"""

                echo downloadSpec
                rtServer.download (downloadSpec, buildInfo)
                if (fileExists('jdk/jdk-8-linux-x64.tar.gz') && fileExists('tomcat/apache-tomcat-8.tar.gz')) {
                    println "Downloaded dependencies"
                } else {
                    println "Missing Dependencies either jdk or tomcat - see listing below:"
                    sh 'ls -d */*'
                    throw new FileNotFoundException("Missing Dependencies")
                }
            } catch (Exception e) {
                println "Caught exception during resolution.  Message ${e.message}"
                throw e
            }
        }
    }
    //Build docker image named "docker-framework" with Java 8 and Tomcat  
    stage('Build') {
        dir ('project-examples/step2_dockertemplate') {
            updateBaseDockerFile(artdocker_registry)
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                    def rtDocker = Artifactory.docker server: rtServer
                    tagName = "${artdocker_registry}/${REPO}/docker-framework:${env.BUILD_NUMBER}"
                    docker.withRegistry ("http://${artdocker_registry}/${REPO}", CREDENTIALS) {
                         sh "docker login -u ${USERNAME} -p ${PASSWORD} http://${artdocker_registry}/${REPO}"
                         docker.build(tagName)
                    }
                    sleep 2
                    buildInfo = rtDocker.push(tagName, REPO, buildInfo)
                    sleep 2
                    rtServer.publishBuildInfo buildInfo
            }
        }
    }
    //Test docker image
    stage('Test') {
        dir('project-examples/step2_dockertemplate/framework-test') {
            def gradleLatestPath = getLatestGradleWar(SERVER_URL).trim()
                echo '1111'

            def gradleWarDownload = """{
            "files": [
                {
                  "pattern": "maven-release-local/${gradleLatestPath}/*.war",
                  "target": "war/webservice.war",
                  "flat": "true"
                }
              ]
            }"""

            rtServer.download(gradleWarDownload)
            updateBaseDockerFile(artdocker_registry)
            updateDockerFile()
            def tagDockerFramework = "${artdocker_registry}/${REPO}/docker-framework-test:${env.BUILD_NUMBER}"
            docker.build(tagDockerFramework)
            if (testFramework(tagDockerFramework)) {
                println "Setting property and promotion"
                updateProperty ("functional-test=pass", SERVER_URL, SOURCE_REPO )
                sh "docker rmi ${tagName}"
            } else {
                updateProperty ("functional-test=fail;failed-test=page-not-loaded", SERVER_URL, SOURCE_REPO)
                currentBuild.result = 'UNSTABLE'
                sh "docker rmi ${tagName}"
                return
            }
        }
    }
    //Scan build's Artifacts in Xray
    stage('Xray Scan') {
        if (XRAY_SCAN == "YES") {
            def xrayConfig = [
                'buildName'     : env.JOB_NAME,
                'buildNumber'   : env.BUILD_NUMBER,
                'failBuild'     : false
            ]
            def xrayResults = rtServer.xrayScan xrayConfig
            echo xrayResults as String
        } else {
            println "No Xray scan performed. To enable set XRAY_SCAN = YES"
        }
        sleep 5
    }
    
    //Promote image from local staging repositoy to production repository 
    stage ('Promote') {

        dir ('project-examples/step2_dockertemplate') {
            promoteBuild (SOURCE_REPO, PROMOTE_REPO, SERVER_URL)
            
            // create latest tag on both source and promote target repo 
            reTagLatest (SOURCE_REPO, PROMOTE_REPO, SOURCE_REPO, SERVER_URL)
            reTagLatest (PROMOTE_REPO, PROMOTE_REPO, SOURCE_REPO, SERVER_URL)
         }
    }
}

def updateBaseDockerFile (artdocker_registry) {
      sh "sed -i -e 's#jfrog.local:5002#${artdocker_registry}/${REPO}#' Dockerfile"
}

def updateDockerFile () {
    def BUILD_NUMBER = env.BUILD_NUMBER
    sh 'sed -i "s/docker-framework:latest/docker-framework:$BUILD_NUMBER/" Dockerfile'
}

def reTagLatest (targetRepo, promote_repo, source_repo, server_url) {
     def BUILD_NUMBER = env.BUILD_NUMBER
     sh 'sed -E "s/@/$BUILD_NUMBER/" retag.json > retag_out.json'
     switch (targetRepo) {
          case promote_repo :
              println "Tagging " + promote_repo + " with latest"
              sh ("""
                sed -E "s/TARGETREPO/${promote_repo}/g" retag_out.json > retaga_out.json
                """)
              break
          case source_repo :
               println "Tagging " + source_repo + " with latest"
               sh ("""
                 sed -E "s/TARGETREPO/${source_repo}/" retag_out.json > retaga_out.json
                """)
               break
      }
      sh 'cat retaga_out.json'
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
          def curlString = "curl -u " + env.USERNAME + ":" + env.PASSWORD + " " + server_url
          def regTagStr = curlString +  "/api/docker/$targetRepo/v2/promote -X POST -H 'Content-Type: application/json' -T retaga_out.json"
          println "Curl String is " + regTagStr
          sh regTagStr
      }
}
//test docker image by runnning container
def testFramework (tag) {
    def result = true
    docker.image(tag).withRun('-p 8181:8181') {c ->
        sleep 10
        def stdout = sh(script: 'curl "http://localhost:8181/swampup/"', returnStdout: true)
        if (stdout.contains("欢迎参加 JFrog")) {
            println "*** Passed Test: " + stdout
        } else {
            println "*** Failed Test: " + stdout
            result = false
        }
    }
    sh "docker rmi ${tag}"
    return result
}

def updateProperty (property, artserverUrl, sourceRepo) {
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
            def curlString = "curl -u " + env.USERNAME + ":" + env.PASSWORD + " " + "-X PUT " + artserverUrl
            def updatePropStr = curlString +  "/api/storage/${sourceRepo}/docker-framework/${env.BUILD_NUMBER}?properties=${property}"
            println "Curl String is " + updatePropStr
            sh updatePropStr
     }
}


def promoteBuild (source_repo, promote_repo, SERVER_URL) {

    def buildPromotion = """ {
        "status"      : "Released",
        "comment"     : "Framework test with latest version of application",
        "ciUser"      : "jenkins",
        "sourceRepo"  : "${source_repo}",
        "targetRepo"  : "${promote_repo}",
        "copy"        : true,
        "dependencies" : false,
        "failFast": true
    }"""

    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
        def createPromo = ["curl", "-X", "POST", "-H", "Content-Type: application/json", "-d", "${buildPromotion }", "-u", "${env.USERNAME}:${env.PASSWORD}", "${SERVER_URL}/api/build/promote/${env.JOB_NAME}/${env.BUILD_NUMBER}"]
        try {
           def getPromoResponse = createPromo.execute().text
           def jsonSlurper = new JsonSlurper()
           def promoStatus = jsonSlurper.parseText("${getPromoResponse}")
           if (promoStatus.error) {
               println "Promotion failed: " + promoStatus
           }
        } catch (Exception e) {
           println "Promotion failed: ${e.message}"
        }
    }
}

def getLatestGradleWar (SERVER_URL) {
    def response = ''
    def gradleLatestWarSrc = """items.find(
    {
        "repo":{"\$eq":"maven-release-local"}, "name":{"\$match":"multi3-*.war"},"@build.name":"maventest"
    }
).sort({"\$desc" : ["created"]}).limit(1)"""

    echo '1111'

    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: CREDENTIALS, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
        def curlString = "curl -s -u " + env.USERNAME + ":" + env.PASSWORD + " " + SERVER_URL
        def gradleLatestStr = curlString +  "/api/search/aql -X POST -H 'Content-Type: text/plain' -d '" + gradleLatestWarSrc + "' | python -c 'import sys, json; print json.load(sys.stdin)[\"results\"][0][\"path\"]'"
        echo '222'

        println "Curl String is " + gradleLatestStr
        response = sh (script: gradleLatestStr, returnStdout: true)
    }
    println "Curl response: " + response
    return response
}

