--------------
timeout
--------------
pipeline {
    agent any

	options { timeout(time: 5, unit: 'SECONDS') }
	stages {
	    stage('获取代码') {
		    steps {
			    script {
                        echo "获取代码出错啦！请您检查项目的git地址"
                        sleep 10
					}	
				}
		 	}
		}
}



--------------
retry
--------------
pipeline {
    agent any
    
 options { retry(3) }
 stages {
     stage('获取代码') {
         //options { retry(3) }
      steps {
       script {
                        sh "lss /etc/"
     } 
    }
    }
  }
}
--------------
post、email
--------------

pipeline{
    agent any
    stages{
        stage('获取代码'){
            steps{
                emailext body: 'hahahahaha', subject: 'test', to: 'bj337681079@sina.com'
            }
        }
    }
    post{
        fixed{
            echo "构建完成goujianshibai"
        }
        success{
            echo "goujian构建wanc"
        }
        always{
            echo "I am here"
        }
    }

}


--------------
parallel 并发
--------------

pipeline{
    agent any
    stages{
        stage('获取代码'){
            steps{
                echo "hello world"
				sleep 10
            }
        }
        stage('test'){
            parallel{
                stage('barch A'){
                    steps {
                        echo "on Branch A"
                    }
                }
                stage('Branch B'){
                    steps{
                        echo "On Branch B"
                    }
                }
            }
        }    
    }
}


--------------
docker/agent
--------------


pipeline {
    agent { docker 'nginx' lable 'linux' } 
    stages {
        stage('Example Build') {
            steps {
                sh 'mvn -B clean verify'
            }
        }
    }
}
